package com.ttpsc.reactionspro.project.rest;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.ttpsc.reactionspro.project.model.bean.DisabledReactionBean;
import com.ttpsc.reactionspro.project.model.bean.ProjectReactionBean;
import com.ttpsc.reactionspro.project.service.DisabledReactionService;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.reaction.service.ReactionService;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Scanned
@Path("/project/{projectKey}")
public class DisabledReactionsRest {

    private final DisabledReactionService disabledReactionService;
    private final ReactionService reactionService;
    private final IssueManager issueManager;

    @Inject
    public DisabledReactionsRest(DisabledReactionService disabledReactionService,
                                 ReactionService reactionService,
                                 @ComponentImport IssueManager issueManager) {
        this.disabledReactionService = disabledReactionService;
        this.reactionService = reactionService;
        this.issueManager = issueManager;
    }

    @PUT
    @Path("/reaction/{reactionId}/disable")
    @Produces({MediaType.APPLICATION_JSON})
    public Response disableReactionInProject(
            @PathParam("projectKey") String projectKey,
            @PathParam("reactionId") Integer reactionId) {

        boolean isDisabled = this.disabledReactionService.disableReactionInProject(projectKey, reactionId);

        return isDisabled
                ? Response.ok().build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/reaction/{reactionId}/enable")
    @Produces({MediaType.APPLICATION_JSON})
    public Response enableReactionInProject(
            @PathParam("projectKey") String projectKey,
            @PathParam("reactionId") Integer reactionId) {

        boolean isEnabled = this.disabledReactionService.enableReactionInProject(projectKey, reactionId);

        return isEnabled
                ? Response.ok().build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

//    @GET
//    @Path("/enabled")
//    @Produces({MediaType.APPLICATION_JSON})
//    public Response getAvailableReactionsForProject(@PathParam("projectKey") String projectKey) {
//        List<ReactionBean> allReactions = this.reactionService.getAllReactions();
//        List<Integer> allDisabledReactionsIdsForProject =
//                this.disabledReactionService.getAllDisabledReactionsForProject(projectKey)
//                        .stream()
//                        .map(DisabledReactionBean::getReactionId)
//                        .collect(Collectors.toList());
//
//        List<ReactionBean> availableReactions = allReactions.stream()
//                .filter(reaction ->
//                        !allDisabledReactionsIdsForProject.contains(reaction.getID())
//                ).collect(Collectors.toList());
//
//        return Response.ok(availableReactions).build();
//    }

    @GET
    @Path("/enabled")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAvailableReactionsForIssue(@PathParam("projectKey") String issueKey) {
        //yup, we are getting here passing here issuekey instead of projectkey, quick mega dirty workaround :D
        List<ReactionBean> allReactions = this.reactionService.getAllReactions();
        Issue issue = issueManager.getIssueObject(issueKey);
        Project project = issue.getProjectObject();
        String projectKey = project.getKey();
        List<Integer> allDisabledReactionsIdsForProject =
                this.disabledReactionService.getAllDisabledReactionsForProject(projectKey)
                        .stream()
                        .map(DisabledReactionBean::getReactionId)
                        .collect(Collectors.toList());

        List<ReactionBean> availableReactions = allReactions.stream()
                .filter(reaction ->
                        !allDisabledReactionsIdsForProject.contains(reaction.getID())
                ).collect(Collectors.toList());

        return Response.ok(availableReactions).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllReactionsForProject(@PathParam("projectKey") String projectKey) {
        List<ReactionBean> allReactions = this.reactionService.getAllReactions();
        List<Integer> allDisabledReactionsIdsForProject =
                this.disabledReactionService.getAllDisabledReactionsForProject(projectKey)
                        .stream()
                        .map(DisabledReactionBean::getReactionId)
                        .collect(Collectors.toList());

        List<ProjectReactionBean> projectReactions = allReactions.stream()
                .map(reaction -> new ProjectReactionBean(
                        reaction,
                        !allDisabledReactionsIdsForProject.contains(reaction.getID())
                ))
                .collect(Collectors.toList());

        return Response.ok(projectReactions).build();
    }

}
