package com.ttpsc.reactionspro.project.service;

import com.ttpsc.reactionspro.project.db.dao.DisabledReactionsDAO;
import com.ttpsc.reactionspro.project.db.entity.DisabledReactionEntity;
import com.ttpsc.reactionspro.project.model.bean.DisabledReactionBean;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class DisabledReactionService {

    private final DisabledReactionsDAO disabledReactionsDAO;

    @Inject
    public DisabledReactionService(DisabledReactionsDAO disabledReactionsDAO) {

        this.disabledReactionsDAO = disabledReactionsDAO;
    }

    public List<DisabledReactionBean> getAllDisabledReactionsForProject(String projectKey) {
        return this.disabledReactionsDAO.getAllForProject(projectKey)
                .stream()
                .map(DisabledReactionBean::of)
                .collect(Collectors.toList());
    }

    private boolean isReactionDisabledInProject(String projectKey, Integer reactionId) {
        List<DisabledReactionEntity> disabledReactions = this.disabledReactionsDAO.getAllForProject(projectKey);

        return disabledReactions.stream().anyMatch(reaction -> reaction.getReactionId().equals(reactionId));
    }

    public boolean disableReactionInProject(String projectKey, Integer emotionId) {
        return this.disabledReactionsDAO.create(projectKey, emotionId).isPresent();
    }

    public boolean enableReactionInProject(String projectKey, Integer emotionId) {
        return isReactionDisabledInProject(projectKey, emotionId) && this.disabledReactionsDAO.delete(projectKey, emotionId);
    }
}
