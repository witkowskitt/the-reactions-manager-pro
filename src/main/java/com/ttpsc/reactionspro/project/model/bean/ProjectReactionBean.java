package com.ttpsc.reactionspro.project.model.bean;

import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class ProjectReactionBean {
    ReactionBean reactionBean;
    Boolean isVisible;
}
