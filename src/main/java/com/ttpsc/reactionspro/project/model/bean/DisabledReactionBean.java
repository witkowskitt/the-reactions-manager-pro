package com.ttpsc.reactionspro.project.model.bean;

import com.ttpsc.reactionspro.project.db.entity.DisabledReactionEntity;
import java.util.Optional;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class DisabledReactionBean {
    Integer id;
    String projectKey;
    Integer reactionId;

    public static DisabledReactionBean of(DisabledReactionEntity entity) {
        return new DisabledReactionBean(
                entity.getID(),
                entity.getProjectKey(),
                entity.getReactionId()
        );
    }

    public static Optional<DisabledReactionBean> of(Optional<DisabledReactionEntity> entity) {
        return entity.map(DisabledReactionBean::of);
    }
}
