package com.ttpsc.reactionspro.project.db.entity;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("DisabledProjReact")
public interface DisabledReactionEntity extends Entity {

    String ID = "ID";
    String PROJECT_KEY = "PROJECT_KEY";
    String REACTION_ID = "REACTION_ID";

    String getProjectKey();

    void setProjectKey(String projectKey);

    Integer getReactionId();

    void setReactionId(Integer reactionId);

}
