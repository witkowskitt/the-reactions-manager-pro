package com.ttpsc.reactionspro.project.db.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.ttpsc.reactionspro.project.db.entity.DisabledReactionEntity;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;
import net.java.ao.DBParam;

@Named
public class DisabledReactionsDAO {
    private final ActiveObjects activeObjects;

    @Inject
    public DisabledReactionsDAO(ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    public Optional<DisabledReactionEntity> create(String projectKey, Integer reactionId) {
        DisabledReactionEntity entity = activeObjects.create(
                DisabledReactionEntity.class,
                new DBParam(DisabledReactionEntity.PROJECT_KEY, projectKey),
                new DBParam(DisabledReactionEntity.REACTION_ID, reactionId)
        );

        return Optional.ofNullable(entity);
    }

    public List<DisabledReactionEntity> getAll() {

        return Arrays.asList(activeObjects.find(DisabledReactionEntity.class));
    }

    public List<DisabledReactionEntity> getAllForProject(String projectKey) {

        return this.getAll().stream()
                .filter(entity -> entity.getProjectKey().equalsIgnoreCase(projectKey))
                .collect(Collectors.toList());
    }

    public Optional<DisabledReactionEntity> getForProject(String projectKey, Integer reactionId) {

        return this.getAllForProject(projectKey)
                .stream()
                .filter(entity -> entity.getReactionId().equals(reactionId))
                .findFirst();
    }

    public boolean delete(String projectKey, Integer reactionId) {
        Optional<DisabledReactionEntity> entity = this.getForProject(projectKey, reactionId);

        if (!entity.isPresent()) {
            return false;
        }

        activeObjects.delete(entity.get());

        return true;
    }


}
