package com.ttpsc.reactionspro.manager.vote.beans;

import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class VoteCounter {
    private ReactionBean reactionBean;
    private Integer count;

    public void incrementCount() {
        ++count;
    }
}
