package com.ttpsc.reactionspro.reaction.model.bean;

import com.ttpsc.reactionspro.reaction.db.entity.ReactionEntity;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class ReactionBean {
    private Integer iD;
    private String imagePath;
    private String displayName;

    public static ReactionBean of(ReactionEntity entity) {
        return new ReactionBean(
                entity.getID(),
                entity.getImagePath(),
                entity.getDisplayName()
        );
    }

    public static Optional<ReactionBean> of(Optional<ReactionEntity> entity) {
        return entity.map(ReactionBean::of);
    }
}
