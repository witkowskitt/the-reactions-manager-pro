package com.ttpsc.reactionspro.reaction.service;

import com.ttpsc.reactionspro.reaction.db.dao.ReactionDAO;
import com.ttpsc.reactionspro.reaction.db.entity.ReactionEntity;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionCreateBean;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ReactionService {
    private final ReactionDAO reactionDAO;

//    private List<ReactionBean> reactionBeans;

    @Inject
    public ReactionService(ReactionDAO reactionDAO) {
        this.reactionDAO = reactionDAO;
//        this.reactionBeans = new ArrayList<>();
//        reactionBeans.add(new ReactionBean(1, "https://www.google.com/url?sa=i&url=https%3A%2F%2Femojiisland.com%2Fproducts%2Fshh-emoji&psig=AOvVaw13qmoaUxRGviKh1McIQOGc&ust=1592591233690000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiJwuv-i-oCFQAAAAAdAAAAABAD", "Shh..."));
//        reactionBeans.add(new ReactionBean(2, "https://www.google.com/url?sa=i&url=https%3A%2F%2Ffavpng.com%2Fpng_search%2Fdiscord-emoji&psig=AOvVaw13qmoaUxRGviKh1McIQOGc&ust=1592591233690000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiJwuv-i-oCFQAAAAAdAAAAABAI", "Hmm..."));
//        reactionBeans.add(new ReactionBean(3, "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinclipart.com%2Fmaxpin%2FiiTohoh%2F&psig=AOvVaw13qmoaUxRGviKh1McIQOGc&ust=1592591233690000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiJwuv-i-oCFQAAAAAdAAAAABAS", "WoW"));
//        reactionBeans.add(new ReactionBean(4, "https://www.google.com/url?sa=i&url=https%3A%2F%2Fpngtree.com%2Fso%2Fwhatsapp-emoji&psig=AOvVaw13qmoaUxRGviKh1McIQOGc&ust=1592591233690000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiJwuv-i-oCFQAAAAAdAAAAABAm", "I'm sick"));
    }

    public List<ReactionBean> getAllReactions() {
        return this.reactionDAO.getAll().stream()
                .map(ReactionBean::of)
                .collect(Collectors.toList());
    }

    public Optional<ReactionBean> createReaction(ReactionCreateBean reactionBean) {

        Optional<ReactionEntity> entity = this.reactionDAO.create(
                reactionBean.getDisplayName(),
                reactionBean.getImagePath(),
                Date.from(Instant.now()));

        return ReactionBean.of(entity);
    }

    public boolean deleteReaction(Integer reactionId) {
        return this.reactionDAO.delete(reactionId);
    }

    public Optional<ReactionBean> getReaction(Integer reactionId) {
        return ReactionBean.of(this.reactionDAO.getById(reactionId));
    }
}
