package com.ttpsc.reactionspro.reaction.rest;

import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionCreateBean;
import com.ttpsc.reactionspro.reaction.service.ReactionService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/reaction")
public class ReactionRest {

    private final ReactionService reactionService;

    @Inject
    public ReactionRest(ReactionService reactionService) {
        this.reactionService = reactionService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllReactions() {
        List<ReactionBean> reactionBeans = this.reactionService.getAllReactions();

        return Response.ok(reactionBeans).build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createNewReaction(ReactionCreateBean reactionCreateBean) {
        Optional<ReactionBean> reaction = this.reactionService.createReaction(reactionCreateBean);

        return !reaction.isPresent()
                ? Response.status(Response.Status.BAD_REQUEST).build()
                : Response.ok(reaction).build();
    }

    @DELETE
    @Path("/{reactionId}")
    public Response deleteReaction(@PathParam("reactionId") Integer reactionId) {
        boolean isDeleted = this.reactionService.deleteReaction(reactionId);

        return isDeleted
                ? Response.noContent().build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

}
