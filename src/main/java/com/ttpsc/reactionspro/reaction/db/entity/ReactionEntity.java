package com.ttpsc.reactionspro.reaction.db.entity;

import com.ttpsc.reactionspro.vote.db.entity.VoteIssueEntity;
import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("Reaction")
public interface ReactionEntity extends Entity {
    String ID = "ID";
    String DISPLAY_NAME = "DISPLAY_NAME";
    String IMAGE_PATH = "IMAGE_PATH";
    String CREATED_DATE = "CREATED_DATE";

    @NotNull
    String getDisplayName();

    void setDisplayName(String issueId);

    @NotNull
    String getImagePath();

    void setImagePath(String fileName);

    @NotNull
    Date getCreatedDate();

    void setCreatedDate(Date createdDate);

    @OneToMany
    VoteIssueEntity getVoteIssue();
}
