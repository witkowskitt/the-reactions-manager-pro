package com.ttpsc.reactionspro.reaction.db.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.ttpsc.reactionspro.reaction.db.entity.ReactionEntity;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;
import net.java.ao.DBParam;

@Named
public class ReactionDAO {
    private final ActiveObjects activeObjects;

    @Inject
    public ReactionDAO(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    public Optional<ReactionEntity> create(String name,
                                           String fileName,
                                           Date creationDate) {
        ReactionEntity createdEntity = activeObjects.create(ReactionEntity.class,
                new DBParam(ReactionEntity.DISPLAY_NAME, name),
                new DBParam(ReactionEntity.IMAGE_PATH, fileName),
                new DBParam(ReactionEntity.CREATED_DATE, creationDate)
        );

        return Optional.ofNullable(createdEntity);
    }

    public Optional<ReactionEntity> getById(int reactionId) {
        return Optional.ofNullable(activeObjects.get(ReactionEntity.class, reactionId));
    }

    public List<ReactionEntity> getAll() {
        return Arrays.asList(activeObjects.find(ReactionEntity.class));
    }

    public boolean delete(int reactionId) {
        Optional<ReactionEntity> reactionEntity = getById(reactionId);
        if (!reactionEntity.isPresent()) {
            return false;
        }
        activeObjects.delete(reactionEntity.get());
        return true;
    }

    public int deleteAll() {
        ReactionEntity[] reactionEntities = activeObjects.find(ReactionEntity.class);
        activeObjects.delete(reactionEntities);
        return reactionEntities.length;
    }

}
