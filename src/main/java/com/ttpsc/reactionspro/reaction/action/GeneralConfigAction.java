package com.ttpsc.reactionspro.reaction.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import webwork.action.Action;

@Scanned
public class GeneralConfigAction extends JiraWebActionSupport {

    @Override
    protected String doExecute() {
        return Action.SUCCESS;
    }

}