package com.ttpsc.reactionspro.vote.db.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.ttpsc.reactionspro.vote.db.entity.VoteIssueEntity;
import net.java.ao.DBParam;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Named
public class VoteDAO {
    private final ActiveObjects activeObjects;

    @Inject
    public VoteDAO(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    public Optional<VoteIssueEntity> create(String issueKey,
                                            Integer appUserId,
                                            Integer reactionId,
                                            Date creationDate) {
        VoteIssueEntity createdEntity = activeObjects.create(VoteIssueEntity.class,
                new DBParam(VoteIssueEntity.ISSUE_KEY_COLUMN, issueKey),
                new DBParam(VoteIssueEntity.APP_USER_ID_COLUMN, appUserId),
                new DBParam(VoteIssueEntity.REACTION_ID_COLUMN, reactionId),
                new DBParam(VoteIssueEntity.CREATED_DATE_COLUMN, creationDate)
        );
        return Optional.ofNullable(createdEntity);
    }

    public Optional<VoteIssueEntity> getById(int voteId) {
        return Optional.ofNullable(activeObjects.get(VoteIssueEntity.class, voteId));
    }

    public List<VoteIssueEntity> getAll() {
        return Arrays.asList(activeObjects.find(VoteIssueEntity.class));
    }

    public boolean delete(int voteId) {
        Optional<VoteIssueEntity> voteEntity = getById(voteId);
        if (!voteEntity.isPresent()) {
            return false;
        }
        activeObjects.delete(voteEntity.get());
        return true;
    }

    public int deleteAll() {
        VoteIssueEntity[] voteEntities = activeObjects.find(VoteIssueEntity.class);
        activeObjects.delete(voteEntities);
        return voteEntities.length;
    }
}
