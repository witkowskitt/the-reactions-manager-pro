package com.ttpsc.reactionspro.vote.db.entity;

import com.ttpsc.reactionspro.reaction.db.entity.ReactionEntity;
import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("Vote_Issue")
public interface VoteIssueEntity extends Entity {

    String ID = "ID";
    String ISSUE_KEY_COLUMN = "ISSUE_KEY";
    String APP_USER_ID_COLUMN = "APP_USER_ID";
    String REACTION_ID_COLUMN = "REACTION_ID";
    String CREATED_DATE_COLUMN = "CREATED_DATE";

    @NotNull
    String getIssueKey();

    void setIssueKey(String issueKey);

    @NotNull
    Integer getAppUserId();

    void setAppUserId(Integer appUserId);

    @NotNull
    ReactionEntity getReaction();

    void setReaction(ReactionEntity reactionEntity);

    @NotNull
    Date getCreatedDate();

    void setCreatedDate(Date createdDate);

}
