package com.ttpsc.reactionspro.vote.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.ttpsc.reactionspro.manager.vote.beans.VoteCounter;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.reaction.service.ReactionService;
import com.ttpsc.reactionspro.vote.model.bean.VoteBean;
import com.ttpsc.reactionspro.vote.model.bean.VoteCreateBean;
import com.ttpsc.reactionspro.vote.service.VoteService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Scanned
@Path("/vote")
public class VoteRest {

    private final VoteService voteService;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ReactionService reactionService;

    @Inject
    public VoteRest(@ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                    VoteService voteService,
                    ReactionService reactionService) {
        this.voteService = voteService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.reactionService = reactionService;
    }

    @GET
    @Path("/issue/{issueKey}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReactionVotesForIssue(@PathParam("issueKey") String issueKey) {
        List<VoteCounter> reactionVotesForIssue = voteService.getReactionVotesForIssue(issueKey);
        return Response.ok(reactionVotesForIssue).build();
    }

    @PUT
    @Path("/issue/{issueKey}/reaction/{reactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response voteForIssue(@PathParam("issueKey") String issueKey,
                                 @PathParam("reactionId") int reactionId) {
        int userId = jiraAuthenticationContext.getLoggedInUser().getId().intValue();
        ReactionBean reactionBean = reactionService
                .getReaction(reactionId).orElseThrow(() -> new NoSuchElementException("Reaction not found"));
        Optional<VoteBean> vote = voteService.createVote(new VoteCreateBean(issueKey, userId, reactionBean));

        return !vote.isPresent()
                ? Response.status(Response.Status.BAD_REQUEST).build()
                : Response.ok(vote).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllVotes() {
        List<VoteBean> voteBeans = this.voteService.getAllVotes();

        return Response.ok(voteBeans).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Response createNewVote(VoteCreateBean voteCreateBean) {
        Optional<VoteBean> vote = this.voteService.createVote(voteCreateBean);

        return !vote.isPresent()
                ? Response.status(Response.Status.BAD_REQUEST).build()
                : Response.ok(vote).build();
    }

    @DELETE
    @Path("/voteId")
    public Response deleteVote(@QueryParam("voteId") Integer voteId) {
        boolean isDeleted = this.voteService.deleteVote(voteId);

        return isDeleted
                ? Response.noContent().build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

}
