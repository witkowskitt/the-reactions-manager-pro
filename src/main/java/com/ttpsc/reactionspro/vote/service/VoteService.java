package com.ttpsc.reactionspro.vote.service;

import com.ttpsc.reactionspro.manager.vote.beans.VoteCounter;
import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.vote.db.dao.VoteDAO;
import com.ttpsc.reactionspro.vote.db.entity.VoteIssueEntity;
import com.ttpsc.reactionspro.vote.model.bean.VoteBean;
import com.ttpsc.reactionspro.vote.model.bean.VoteCreateBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class VoteService {
    private final VoteDAO voteDAO;

    @Inject
    public VoteService(VoteDAO voteDAO) {
        this.voteDAO = voteDAO;
    }

    public List<VoteBean> getAllVotes() {
        return this.voteDAO.getAll().stream()
                .map(VoteBean::of)
                .collect(Collectors.toList());
    }

    public Optional<VoteBean> createVote(VoteCreateBean voteBean) {
        Optional<VoteIssueEntity> entity = this.voteDAO.create(
                voteBean.getIssueKey(),
                voteBean.getUserId(),
                voteBean.getReactionBean().getID(),
                Date.from(Instant.now()));

        return VoteBean.of(entity);
    }

    public boolean deleteVote(Integer voteId) {
        return voteDAO.delete(voteId);
    }

    public Optional<VoteBean> getVote(Integer voteId) {
        return VoteBean.of(voteDAO.getById(voteId));
    }

    public List<VoteCounter> getReactionVotesForIssue(String issueKey) {
        List<VoteCounter> result = new ArrayList<>();
        List<VoteBean> allIssueVotes = getAllIssueVotesByIssueKey(issueKey);
        allIssueVotes.forEach(voteBean -> {
            ReactionBean reactionBean = voteBean.getReactionBean();
            if (isReactionExistInVoteCounterList(reactionBean, result)) {
                result.stream()
                        .filter(vC -> vC.getReactionBean().equals(reactionBean))
                        .findFirst()
                        .ifPresent(VoteCounter::incrementCount);
            } else {
                result.add(new VoteCounter(reactionBean, 1));
            }
        });
        return result;
    }

    public List<VoteBean> getAllIssueVotesByIssueKey(String issueKey) {
        List<VoteBean> allIssueVotes = new ArrayList<>();
        getAllVotes().forEach(voteBean -> {
            if (voteBean.getIssueKey().equals(issueKey)) {
                allIssueVotes.add(voteBean);
            }
        });
        return allIssueVotes;
    }

    private boolean isReactionExistInVoteCounterList(ReactionBean reactionBean,
                                                     List<VoteCounter> voteCounterList) {
        boolean result = false;
        for (VoteCounter voteCounter : voteCounterList) {
            if (voteCounter.getReactionBean().equals(reactionBean)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
