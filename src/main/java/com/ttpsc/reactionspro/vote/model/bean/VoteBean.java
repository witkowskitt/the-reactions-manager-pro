package com.ttpsc.reactionspro.vote.model.bean;

import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import com.ttpsc.reactionspro.vote.db.entity.VoteIssueEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Optional;

@Data
@AllArgsConstructor
public class VoteBean {
    private Integer id;
    private String issueKey;
    private Integer userId;
    private ReactionBean reactionBean;

    public static VoteBean of(VoteIssueEntity entity) {
        return new VoteBean(
                entity.getID(),
                entity.getIssueKey(),
                entity.getAppUserId(),
                ReactionBean.of(entity.getReaction())
        );
    }

    public static Optional<VoteBean> of(Optional<VoteIssueEntity> entity) {
        return entity.map(VoteBean::of);
    }
}
