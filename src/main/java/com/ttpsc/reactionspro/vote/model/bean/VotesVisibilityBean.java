package com.ttpsc.reactionspro.vote.model.bean;

import lombok.Data;

import java.util.List;

@Data
public class VotesVisibilityBean {
    public List<Integer> visibleVoteIds;
}
