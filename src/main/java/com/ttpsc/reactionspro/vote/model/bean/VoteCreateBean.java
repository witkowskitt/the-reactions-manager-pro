package com.ttpsc.reactionspro.vote.model.bean;

import com.ttpsc.reactionspro.reaction.model.bean.ReactionBean;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VoteCreateBean {
    private String issueKey;
    private Integer userId;
    private ReactionBean reactionBean;
}
