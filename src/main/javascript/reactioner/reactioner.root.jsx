import React from 'react';
import ReactDOM from 'react-dom';
import Reactioner from './container/reactioner.container';

function canRender() {
  return !!document.getElementById('summary-val');
}

function getRenderElement() {
  const newNode = document.createElement('span');
  const container = document.getElementById('summary-val');
  container.parentNode.insertBefore(newNode, container.nextSibling);
  return newNode;
}

AJS.toInit(() => {
  if (canRender()) {
    ReactDOM.render(
      <Reactioner />,
      getRenderElement(),
    );
  }
});
