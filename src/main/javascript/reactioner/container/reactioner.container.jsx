import React from 'react';
import { getReactionsForIssue, getVotedReactionsForIssue, voteForIssue } from '../api/vote.api';
import AvailableReactionsList from '../component/availableReactionsList.component';
import VotedReactionsList from '../component/votedReactionsList.component';

class Reactioner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      availableReactions: [],
      votedReactions: [],
    };
  }

  async componentDidMount() {
    await this.reloadAvailableReactionsList();
    await this.reloadVotedReactionsList();
  }

  async reloadAvailableReactionsList() {
    const availableReactions = await getReactionsForIssue(JIRA.Issue.getIssueKey());
    this.setState({ availableReactions });
  }

  async reloadVotedReactionsList() {
    const votedReactions = await getVotedReactionsForIssue(JIRA.Issue.getIssueKey());
    this.setState({ votedReactions });
  }

  async vote(reactionId) {
    await voteForIssue(JIRA.Issue.getIssueKey(), reactionId);
    await this.reloadVotedReactionsList();
  }

  render() {
    return (
      <div>
        <VotedReactionsList reactions={this.state.votedReactions} />
        {/* <div style={{ */}
        {/*  display: 'block', height: '1px', border: '0', borderTop: '1px solid #ccc', */}
        {/* }} */}
        {/* /> */}
        <div style={{ marginTop: '5px' }}>
          <AvailableReactionsList
            reactions={this.state.availableReactions}
            onClick={(reactionId) => this.vote(reactionId)}
          />
        </div>
        <div />
      </div>
    );
  }
}

export default Reactioner;
