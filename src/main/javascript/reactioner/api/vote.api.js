export async function getReactionsForIssue(issueKey) {
  const response = await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/project/${issueKey}/enabled`);
  return response.json();
}

export async function getVotedReactionsForIssue(issueKey) {
  const response = await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/vote/issue/${issueKey}`);
  return response.json();
}

export async function voteForIssue(issueKey, reactionId) {
  await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/vote/issue/${issueKey}/reaction/${reactionId}`, {
    method: 'PUT',
  });
}
