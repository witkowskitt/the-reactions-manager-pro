import React from 'react';
import PropTypes from 'prop-types';
import Reaction from '../../reaction/component/reaction.component';

class VotedReactionsList extends React.Component {
  render() {
    const reactionsTable = this.props.reactions.map((reaction) => (
      <span
        style={{
          margin: '2px', padding: '2px',
        }}
      >
        <Reaction url={reaction.reactionBean.imagePath} size={15} />
        <span>
          (
          {reaction.count}
          )
        </span>
      </span>
    ));

    return (
      <h4>
        Reactions
        {' '}
        {reactionsTable}
      </h4>
    );
  }
}

VotedReactionsList.propTypes = {
  reactions: PropTypes.array,
};

export default VotedReactionsList;
