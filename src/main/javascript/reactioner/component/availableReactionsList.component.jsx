import React from 'react';
import PropTypes from 'prop-types';
import Reaction from '../../reaction/component/reaction.component';

class AvailableReactionsList extends React.Component {
  render() {
    const reactionsTable = this.props.reactions.map((reaction) => (
      <a
        href='#'
        style={{
          border: '1px solid gray', borderRadius: '5px', margin: '2px', padding: '2px',
        }}
        onClick={() => (this.props.onClick ? this.props.onClick(reaction.iD) : null)}
      >
        <Reaction url={reaction.imagePath} size={15} />
        {' '}
        {reaction.displayName}
        {' '}
      </a>
    ));

    return (
      <div>
        <img src='https://i.imgur.com/un4gSu2.png' alt=' ' style={{ width: '20px', height: '18px', marginTop: '2px' }} />
        {reactionsTable}
      </div>
    );
  }
}

AvailableReactionsList.propTypes = {
  reactions: PropTypes.array,
  onClick: PropTypes.func,
};

export default AvailableReactionsList;
