import React from 'react';
import PropTypes from 'prop-types';

class Reaction extends React.Component {
  render() {
    const size = this.props.size?`${this.props.size}px`:'30px';
    return (
      <div style={{ display: 'inline-block' }}>
        <img src={this.props.url} alt=' ' style={{ width: size, height: size }} />
      </div>
    );
  }
}

Reaction.propTypes = {
  url: PropTypes.string.isRequired,
  size: PropTypes.number,
};

export default Reaction;
