export async function getAllReactions() {
  const response = await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/reaction`);
  return response.json();
}

export async function getReactionsForProject(projectKey) {
  const response = await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/project/${projectKey}`);
  return response.json();
}

export async function addReaction(imagePath, displayName) {
  const data = {
    imagePath,
    displayName,
  };

  const response = await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/reaction`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  return response.json();
}

export async function enableReactionInProject(projectKey, reactionId) {
  await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/project/${projectKey}/reaction/${reactionId}/enable`, {
    method: 'PUT',
    // headers: {
    //   'Content-Type': 'application/json',
    // },
  });
}

export async function disableReactionInProject(projectKey, reactionId) {
  await fetch(`${AJS.contextPath()}/rest/reactionspro/1.0/project/${projectKey}/reaction/${reactionId}/disable`, {
    method: 'PUT',
    // headers: {
    //   'Content-Type': 'application/json',
    // },
  });
}
