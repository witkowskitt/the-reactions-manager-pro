import React from 'react';
import PropTypes from 'prop-types';
import Reaction from '../../reaction/component/reaction.component';
import { disableReactionInProject, enableReactionInProject } from '../api/reaction.api';

class ProjectReactionsList extends React.Component {
  async onVisibilityChange(reactionId, visible) {
    if (!visible) {
      await disableReactionInProject(this.props.projeckKey, reactionId);
    } else {
      await enableReactionInProject(this.props.projeckKey, reactionId);
    }
    if (this.props.onChange) {
      this.props.onChange();
    }
  }

  render() {
    const reactionsTable = this.props.reactions.map((reaction) => (
      <div>
        <input
          type='checkbox'
          checked={reaction.isVisible ? 'checked' : ''}
          onChange={(evt) => this.onVisibilityChange(reaction.reactionBean.iD, evt.currentTarget.checked)}
        />
        <Reaction url={reaction.reactionBean.imagePath} />
        {' '}
        {reaction.reactionBean.displayName}
      </div>
    ));

    return (
      <div>
        <h2>Available reactions:</h2>
        {reactionsTable}
      </div>
    );
  }
}

ProjectReactionsList.propTypes = {
  reactions: PropTypes.array,
  projeckKey: PropTypes.string,
  onChange: PropTypes.func,
};

export default ProjectReactionsList;
