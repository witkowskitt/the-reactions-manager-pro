import React from 'react';
import PropTypes from 'prop-types';
import { addReaction } from '../api/reaction.api';

class AddReaction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      name: '',
    };
  }

  onNameChange(name) {
    this.setState({ name });
  }

  onUrlChange(url) {
    this.setState({ url });
  }

  async onAddClick() {
    await addReaction(
      this.state.url,
      this.state.name,
    );
    if (this.props.onAdd) {
      this.props.onAdd();
    }
  }

  render() {
    return (
      <div>
        <h2>Add new reaction:</h2>
        <div>
          Url:
          <input
            value={this.state.url}
            onChange={(evt) => this.onUrlChange(evt.currentTarget.value)}
          />
        </div>
        <div>
          Name:
          <input
            value={this.state.name}
            onChange={(evt) => this.onNameChange(evt.currentTarget.value)}
          />
        </div>
        <div>
          <button value='Add' type='submit' onClick={() => this.onAddClick()}>Add</button>
        </div>
      </div>
    );
  }
}

AddReaction.propTypes = {
  onAdd: PropTypes.func,
};

export default AddReaction;
