import React from 'react';
import PropTypes from 'prop-types';
import Reaction from '../../reaction/component/reaction.component';

class ReactionsList extends React.Component {
  render() {
    const reactionsTable = this.props.reactions.map((reaction) => (
      <div>
        <Reaction url={reaction.imagePath} />
        {' '}
        {reaction.displayName}
      </div>
    ));

    return (
      <div>
        <h2>Available reactions:</h2>
        {reactionsTable}
      </div>
    );
  }
}

ReactionsList.propTypes = {
  reactions: PropTypes.array,
};

export default ReactionsList;
