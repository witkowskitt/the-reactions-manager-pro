import React from 'react';
import AddReaction from '../component/addReaction.component';
import ReactionsList from '../component/reactionsList.component';
import { getAllReactions } from '../api/reaction.api';

class Config extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reactions: [],
    };
  }

  async componentDidMount() {
    await this.reloadReactionsList();
  }

  async reloadReactionsList() {
    const reactions = await getAllReactions();
    this.setState({ reactions });
  }

  render() {
    return (
      <div>
        <h1>Edit global reactions</h1>
        <ReactionsList reactions={this.state.reactions} />
        <div />
        <AddReaction onAdd={() => this.reloadReactionsList()} />
      </div>
    );
  }
}

export default Config;
