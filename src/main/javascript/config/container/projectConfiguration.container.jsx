import React from 'react';
import PropTypes from 'prop-types';
import AddReaction from '../component/addReaction.component';
import { getReactionsForProject } from '../api/reaction.api';
import ProjectReactionsList from '../component/projectReactionsList.component';

class ProjectConfig extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reactions: [],
    };
  }

  async componentDidMount() {
    await this.reloadReactionsList();
  }

  async reloadReactionsList() {
    const reactions = await getReactionsForProject(this.props.projectKey);
    this.setState({ reactions });
  }

  render() {
    return (
      <div>
        <h1>
          Edit reactions for project
          {' '}
          {this.props.projectKey}
        </h1>
        <ProjectReactionsList
          reactions={this.state.reactions}
          projeckKey={this.props.projectKey}
          onChange={() => this.reloadReactionsList()}
        />
        <div />
        <AddReaction onAdd={() => this.reloadReactionsList()} />
      </div>
    );
  }
}

ProjectConfig.propTypes = {
  projectKey: PropTypes.number,
};

export default ProjectConfig;
