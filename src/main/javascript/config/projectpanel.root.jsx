import React from 'react';
import ReactDOM from 'react-dom';
import ProjectConfig from './container/projectConfiguration.container';

function getProjectKey() {
  return window.location.search.split('=')[1]; // yolo
}

AJS.toInit(() => {
  ReactDOM.render(
    <ProjectConfig projectKey={getProjectKey()} />,
    document.getElementById('root'),
  );
});
