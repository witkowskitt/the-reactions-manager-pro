import React from 'react';
import ReactDOM from 'react-dom';
import Config from './container/configuration.container';

AJS.toInit(() => {
  ReactDOM.render(
    <Config />,
    document.getElementById('root'),
  );
});
