const helpers = require('./helper');

module.exports = {
  stats: {
    warnings: false,
  },
  entry: {
    reactioner: ['@babel/polyfill', helpers.sourceRoot('reactioner/reactioner.root.jsx')],
    config: ['@babel/polyfill', helpers.sourceRoot('config/config.root.jsx')],
    projectpanel: ['@babel/polyfill', helpers.sourceRoot('config/projectpanel.root.jsx')],
  },
  output: {
    path: helpers.targetRoot('js'),
    filename: '[name].bundle.js',
    libraryTarget: 'var',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|__tests__|lib)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/react'],
            configFile: helpers.root('config/.babelrc'),
          },
        },
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules|lib/,
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          failOnWarning: false,
          failOnError: true,
          fix: true,
          configFile: 'config/.eslintrc.js',
        },
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff)$/,
        exclude: /node_modules/,
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.(svg)$/i,
        exclude: /node_modules/,
        loader: 'file-loader?name=/icons/[name].[ext]',
      },
      {
        test: /\.jpg$/,
        exclude: /node_modules/,
        loader: 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
};
