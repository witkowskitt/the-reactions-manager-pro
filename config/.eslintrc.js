module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: [
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    AP: true,
    jest: true,
    AJS: false,
    JIRA: false,
    VM$: false
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
	  'indent': ['error', 2],
    'no-plusplus': 0,
    'no-underscore-dangle': 0,
    'class-methods-use-this': 0,
    'global-require': 0,
    'no-restricted-globals': 1,
    'no-param-reassign': 1,
    'quotes': 1,
    'quote-props': [1, 'consistent-as-needed'],
    'default-case': 1,
    'no-else-return': 1,
    'max-len': [1, 120, 4, { ignoreComments: true }],
    'linebreak-style': ['off', 'unix'],
    'space-infix-ops': 0,
    'jsx-quotes': ['error', 'prefer-single'],

    'react/jsx-indent': ['error', 2],
    'react/forbid-prop-types': 0,
    'react/jsx-props-no-spreading': 0,
    'react/no-access-state-in-setstate': 1,
    'react/no-array-index-key': 1,
    'react/require-default-props': 1,
    'react/prefer-stateless-function': 1,
    'react/destructuring-assignment': 1,
    'react/no-unused-prop-types': 1,

    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/anchor-is-valid': 1,
    'jsx-a11y/click-events-have-key-events': 1,
    'jsx-a11y/label-has-associated-control': 1,
    'jsx-a11y/no-noninteractive-element-interactions': 1,
  },
};
